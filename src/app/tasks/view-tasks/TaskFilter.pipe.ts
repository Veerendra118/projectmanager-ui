import {Pipe, PipeTransform } from '@angular/core'
import { Task } from 'src/app/Model/task';


@Pipe({
name: 'TaskFilter',
pure:true
})

export class TaskFilterPipe implements PipeTransform{

transform(items: Task[], projectName:string , tName:string, parentId:number):Task[]
        {
         let  filteredTasks: Task[]=items ;
         //filteredTasks =Object.assign({}, items);
         
           if(projectName.length>0)
           filteredTasks = filteredTasks.filter(x=>x.projectName.toLowerCase().includes(projectName.toLowerCase()));

         if(tName.length>0)
           filteredTasks = filteredTasks.filter(x=>x.taskName.toLowerCase().includes(tName.toLowerCase()));

           if(parentId==0) //This is applicable only if we need parent tasks alone
           filteredTasks = filteredTasks.filter(x=>x.parentTaskId == parentId);
         
        // if(pName.length>0)
        //    filteredTasks = filteredTasks.filter(x=>x.parentTaskName.toLowerCase().includes(pName.toLowerCase()));
          
        //   //Priority From and To
        //   if(pFrom>0 && pTo >0)
        //       filteredTasks = filteredTasks.filter(x=>x.priority >=pFrom && x.priority <=pTo);
    
              
        //   if(sDate !=null && sDate !=undefined && eDate !=null && eDate !=undefined)
        //     filteredTasks = filteredTasks.filter(t =>this.ValidateDate( t.startDate, sDate, eDate));


         return filteredTasks;
    }

    ValidateDate(dateToverify:Date, from:Date, to:Date): boolean
    {
        let isvalid:boolean=true;
         var d1 = new Date( dateToverify);
         var d2 = new Date(from) ;
         var d3 = new Date(to) ;

           isvalid =  (d2 <= d1 && d1 <=d3 )     
        
        return isvalid;

    }
}