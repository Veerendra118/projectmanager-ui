import { Component, OnInit } from '@angular/core';
import { TaskApiService } from 'src/app/shared/task-api.service';
import {ProjectApiService} from 'src/app/shared/project-api.service';
import { Task } from 'src/app/Model/task';
import {Project} from 'src/app/Model/project';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import arraySort from 'array-sort';



@Component({
  selector: 'app-view-tasks',
  templateUrl: './view-tasks.component.html',
  styleUrls: ['./view-tasks.component.css']
})

export class ViewTasksComponent implements OnInit {
 
  public datepickerConfig: BsDatepickerConfig;
  public tasks:Task[]=[];
  public p :number =1;
  //public dbTasks:Task[]=[];
  public projects :Project[];

  public filteredPrjName :string;
 
  private _taskId: number;
  private _taskName:string;
  private _parentTaskName:string;
  private _priorityFrom:number;
  private _priorityTo:number;
  private _startDate:Date;
  private _endDate:Date;
  private _projectName :string;

get taskId():number{ return this._taskId;}
set taskId(value:number){ this._taskId=value;}


  get startDate():Date
  {
    return this._startDate;
  }
  set startDate(value:Date)
  {
      this._startDate=value;
  }

  get endDate():Date
  {
    return this._endDate;
  }
  set endDate(value:Date)
  {
      this._endDate=value;
  }

get projectName():string{ return this._projectName;}
set projectName(value:string) {this._projectName=value;}

  get taskName():string
  {
     return this._taskName;
  }
  set taskName(value:string){
      this._taskName =value;
  }

  get parentTaskName():string
  {
     return this._parentTaskName;
  }
  set parentTaskName(value:string){
      this._parentTaskName =value;
  }
  
  get priorityFrom():number{
    return  this._priorityFrom;
  }
  set priorityFrom(value:number){
     this._priorityFrom=value;
  }

  get priorityTo():number{
    return  this._priorityTo;
  }
  set priorityTo(value:number){
     this._priorityTo=value;
  }




  constructor( private _taskApiService: TaskApiService, private _projectApiService:ProjectApiService) { 

        let d =  new Date();
        this.taskId=-1; //This should be -1 only
        this.projectName="";
        this.taskName="";
        this.parentTaskName="";
        this.priorityFrom=1;
        this.priorityTo=30;
        this.filteredPrjName ="";
        this.startDate=new Date(d.getFullYear(),0, 1) ;
        this.endDate=new Date( d.getFullYear()+ 1,4, 1) ;
        
      this.datepickerConfig = new BsDatepickerConfig(); 
      this.datepickerConfig.containerClass='theme-dark-blue';
      this.datepickerConfig.showWeekNumbers=false;
      this.datepickerConfig.dateInputFormat="MM/DD/YYYY";
      this.datepickerConfig.minDate= new Date(2019,0,1);
      this.datepickerConfig.maxDate= new Date(2020,12,31);
      

      }

  ngOnInit() {
    this._taskApiService.GetAllTasks()
         .subscribe(data=>this.tasks=data);
   
         this._projectApiService.GetAllProjects()
         .subscribe(data=>this.projects=data);
 
  }

  public sortByStart()
  {
     arraySort(this.tasks, "startDate");

  }

  public sortByPriority()
  {
     arraySort(this.tasks, "priority");
  }
  public sortByEnd()
  {
     arraySort(this.tasks, "endDate");
  }

  public sortByCompletion()
  {
     arraySort(this.tasks, "status");
  }

  public selectProject( event: any, item: any) {

    
    this.projectName = item.projectName;
    
      
  
  }
  public UnSelectProject()
  {
    this.projectName="";
    this.filteredPrjName="";

  }

  EndOfTask(t:Task)
  {
     if(confirm('Are you sure, you want to end the task:'+ t.taskName +' ?')) 
     {
          t.isTaskEnded=true;
          t.status="CLOSED";
          t.isActive =true;
          this._taskApiService.PutTask(t.taskId,t).subscribe(
            (()=>{console.log('task is endded');
            this._taskApiService.GetAllTasks()
                .subscribe(data =>this.tasks=data);           
          })
          );
     }
  }
}
