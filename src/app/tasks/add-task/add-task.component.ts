import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/Model/task';
import { TaskApiService } from 'src/app/shared/task-api.service';
import {ProjectApiService} from 'src/app/shared/project-api.service';
import {UserApiService} from 'src/app/shared/user-api.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { BsDatepickerConfig, DateFormatter } from 'ngx-bootstrap/datepicker'
import { ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { CanComponentDeactivate } from 'src/app/shared/CanDeactivateGuard';
import { Project } from 'src/app/Model/Project';
import { User } from 'src/app/Model/user';


@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements  OnInit, CanComponentDeactivate {
  @ViewChild('form', { static: true }) 
  public addTaskform:NgForm;
  public datepickerConfig: BsDatepickerConfig;
  public currentTask: Task=new Task();
  public tasks :Task[]=[];
  public projects:Project[]=[];
  public taskUsers :User[]=[];
  public error:any={isError:false, ErrorMessage:"" };
  public isValidDate:any;

  public selectedUserName :string;
  public selectedProjectName :string;
  public selectedTaskName :string;

  

  constructor(private _route:Router
    , private _taskService: TaskApiService
    , private _projectService:ProjectApiService
    , private _userService :UserApiService) {
  
    this.datepickerConfig = new BsDatepickerConfig();
    this.datepickerConfig.containerClass='theme-dark-blue';
    this.datepickerConfig.showWeekNumbers=false;
    this.datepickerConfig.dateInputFormat="DD/MM/YYYY";
    
    this.datepickerConfig.minDate= new Date(2019,12,1);
    this.datepickerConfig.maxDate= new Date(2020,12,31);
    
    this.currentTask = new Task();  
    
    this.selectedUserName ='';
    this.selectedProjectName ='';
    this.selectedTaskName='';

   }

   canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.addTaskform.dirty)
      {
        return confirm('You have unsaved information! Do you want to leave the current screen?');
    }
    return true;
  }

  ngOnInit() {
     this.resetForm(); 
     
     this._taskService.GetAllTasks()
     .subscribe(data =>this.tasks=data);

     this._projectService.GetAllProjects()
     .subscribe(data=>this.projects =data);

    this._userService.GetAllUsers()
     .subscribe(data=>this.taskUsers=data);

     }

     Clear(): void{
       this.resetForm();
       
     }

  resetForm(form?:NgForm) 
  {
    if(form !=null)
    {
     form.resetForm();
         }
       this.currentTask =new Task();
       this.currentTask.taskId=0;
       this.currentTask.taskName='';
       this.currentTask.parentTaskId=0;
       this.currentTask.parentTaskName='';
       this.currentTask.priority=0;
       this.currentTask.startDate = null;
       this.currentTask.endDate = null;
       this.currentTask.status='OPEN';
       this.currentTask.isTaskEnded=false;
       this.currentTask.projectId=0;
       this.currentTask.projectName='';
       this.currentTask.assignedId=0;
       this.currentTask.taskUserName='';
       this.currentTask.isParentTask=false;

       this.currentTask.filterPrjName='';
       this.currentTask.filterTskName='';
      this.currentTask.filterUsrName='';
       
    }

  onsubmit(form : NgForm) : void
  {
    if(this.validate())
    {
      //this.currentTask.startDate =new Date(this.currentTask.startDate.toLocaleDateString());
      //this.currentTask.endDate =new Date(this.currentTask.endDate.toLocaleDateString());
      //alert("Assigned to :"+ this.currentTask.assignedId);
      
      this._taskService.PostTask(this.currentTask).subscribe
       (data=>{
         this.tasks.push(this.currentTask);
         this.resetForm(form);
         this._route.navigate(['/tasks']);
        }
      );
    }
  }

  public selectProject(event: any, item: any) {
    this.currentTask.projectName = item.projectName;
    this.currentTask.projectId=item.projectId;
    this.selectedProjectName=item.projectName;

  }
  public UnSelectProject()
  {
    this.currentTask.projectName = '';
    this.currentTask.projectId=0;
    this.currentTask.filterPrjName='';
    this.selectedProjectName ='';
  }

  public selectUser(event: any, item: any) {
    this.currentTask.taskUserName = item.firstName + " "+ item.lastName;
    this.currentTask.assignedId=item.userId;
    this.selectedUserName =this.currentTask.taskUserName;
  }
  public UnSelectUser()
  {
    this.currentTask.taskUserName = '';
    this.currentTask.assignedId=0;
    this.currentTask.filterUsrName='';
    this.selectedUserName='';
  }

  public IsParentTask() : boolean
  {
    return (this.currentTask.parentTaskId >0);
  }


  public selectParentTask(event: any, item: any) {
    this.currentTask.parentTaskName = item.taskName;
    this.currentTask.parentTaskId=item.taskId;
    this.selectedTaskName =item.taskName;
  }
  public UnSelectParentTask()
  {
    this.currentTask.parentTaskName = '';
    this.currentTask.parentTaskId=0;
    this.currentTask.filterTskName='';
    this.selectedTaskName='';
  }

  validate(): boolean
  {

    if(this.currentTask.isParentTask ==false )
    {
      if( this.currentTask.startDate ==null)
      {
          alert( "Start date is required");
           return false;
      }
      
      if( this.currentTask.endDate ==null)
      {
          alert( "End date is required");
           return false;
      }

      if(this.currentTask.endDate <= this.currentTask.startDate)
      {
            alert('end date should be greater than start date');
            return false;
      }
    }
    else{ // As per requirement when parent task is enabled
      this.currentTask.priority=0;
      this.currentTask.startDate =null;
      this.currentTask.endDate =null;
    }

      return true;
  }
}

  
