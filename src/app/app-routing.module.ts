import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTaskComponent } from 'src/app/tasks/add-task/add-task.component';
import { ViewTasksComponent } from 'src/app/tasks/view-tasks/view-tasks.component';
import { UpdateTaskComponent } from 'src/app/tasks/update-task/update-task.component';
import { PageNotFoundComponent } from 'src/app/tasks/page-not-found/page-not-found.component';
import { CanDeactivateGuard } from './shared/CanDeactivateGuard';
import {ProjectsComponent} from 'src/app/projects/projects.component';
import {UsersComponent} from 'src/app/users/users.component'

const routes: Routes = [
  {path:'',  redirectTo:'/tasks', pathMatch:'full'  },
  {  path:'tasks', 
     component:ViewTasksComponent
 },

 {path:'',  redirectTo:'/projects', pathMatch:'full'  },
  {  path:'projects', 
     component:ProjectsComponent
 },
 {path:'',  redirectTo:'/users', pathMatch:'full'  },
  {  path:'users', 
     component:UsersComponent
 },

  {path:'addtask', 
   component:AddTaskComponent,
   canDeactivate:[CanDeactivateGuard]
   
 },
  {path:'updatetask/:taskId',
     component:UpdateTaskComponent,
     canDeactivate:[CanDeactivateGuard]
     },
  {path:"**", component:PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents =[ViewTasksComponent,AddTaskComponent,UpdateTaskComponent,PageNotFoundComponent ]