import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/Model/user';
import { Observable, of } from 'rxjs';
import {catchError, tap} from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders()
       .append("Content-Type", "application/json; charset=UTF-8")
       .append('Access-Control-Allow-Headers', 'Content-Type')
       .append('Access-Control-Allow-Methods', '*')
       .append('Access-Control-Allow-Origin', '*')
       .set("Accept", 'application/json; charset=UTF-8')
};
 //const apiUrl: string ="http://localhost:53538/api/"; 
const apiUrl:string ="http://localhost/ProjectManagerService/api/";


@Injectable({
  providedIn: 'root'
})
export class UserApiService {

  constructor(private httpClient:HttpClient) { } 
  public errMessage:string="";
  public RootUrl :string =apiUrl;

  public currentUsr : User;
  public userList : User[];

  
  GetAllUsers (): Observable<User[]> {

    return this.httpClient.get<User[]>(apiUrl +"Users")
      .pipe(
        tap(() => console.log('Get All Users'))
      );
  }

  postUser(currentUsr : User){
    return this.httpClient.post(this.RootUrl+'Users',currentUsr);
     
   }
 
   refreshList(){
     this.httpClient.get(this.RootUrl+'Users')
     .toPromise().then(res => this.userList = res as User[]);
   }
 
   putUser(currentUsr : User){

    
     return this.httpClient.put(this.RootUrl+'Users/'+currentUsr.userId,currentUsr);
      
    }
 
    deleteUser(id : number){
      
     return this.httpClient.delete(this.RootUrl+'Users/'+id,httpOptions);
    }
 }

