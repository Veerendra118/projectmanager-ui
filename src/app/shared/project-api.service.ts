import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Project } from 'src/app/Model/Project';
import { Observable, of } from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders()
       .append("Content-Type", "application/json; charset=UTF-8")
       .append('Access-Control-Allow-Headers', 'Content-Type')
       .append('Access-Control-Allow-Methods', '*')
       .append('Access-Control-Allow-Origin', '*')
       .set("Accept", 'application/json; charset=UTF-8')
};
 //const apiUrl: string ="http://localhost:53538/api/"; 
const apiUrl:string ="http://localhost/ProjectManagerService/api/";



@Injectable({
  providedIn: 'root'
})
export class ProjectApiService {

  constructor(private httpClient:HttpClient) { } 
  public errMessage:string="";
  public RootUrl :string =apiUrl;

  public curProject : Project;
  public projectList : Project[];

  private handleError<T> (result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.log(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  GetAllProjects (): Observable<Project[]> {

    return this.httpClient.get<Project[]>(apiUrl +"projects")
      .pipe(
        tap(() => console.log('Get All Projects')),
        catchError(this.handleError([]))
      );
  }
  
  GetProject(projectId:number)
  {
    return  this.httpClient.get<Project>(apiUrl+"projects/"+ projectId);
  }


  postProject(curProject : Project){
    return this.httpClient.post(this.RootUrl+'projects',curProject);
     
   }
 
   refreshList(){
     this.httpClient.get(this.RootUrl+'projects')
     .toPromise().then(res => this.projectList = res as Project[]);
   }
 
   putProject(curProject : Project){
     return this.httpClient.put(this.RootUrl+'projects/'+curProject.projectId,curProject);
      
    }
 
    deleteProject(id : number){
     return this.httpClient.delete(this.RootUrl+'projects/'+id,httpOptions);
    }  




}
