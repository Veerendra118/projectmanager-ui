import { TestBed } from '@angular/core/testing';
import { UserApiService } from './user-api.service';
import {HttpClientModule,HttpClient } from '@angular/common/http';

import { User } from '../Model/user';
import {HttpTestingController, HttpClientTestingModule} from '@angular/common/http/testing';



describe('UserApiService', () => {
  let service: UserApiService;
  let httpMock: HttpTestingController;

  const testUsers: User[] = [{
    userId: 1,
    firstName: 'Ram',
    lastName: 'Kaneria',
    employeeId :1,
    isActive: true,
    userName : 'Ram' + " " + 'Kaneria'
    }, 
    {
      userId: 1,
    firstName: 'Shyam',
    lastName: 'Benegal',
    employeeId :21,
    isActive: true,
    userName : 'Shyam' + " " + 'Benegal'
      }
    ];



  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [HttpClientModule,  HttpClientTestingModule],
        providers: [UserApiService]
        
    });
    service = TestBed.get(UserApiService);
    httpMock =TestBed.get(HttpTestingController);
  });


  it('should be created', () => {
    const service: UserApiService = TestBed.get(UserApiService);
    expect(service).toBeTruthy();
  });

  it('be able to retrieve users from the API bia GET', () => {
    service.GetAllUsers().subscribe(myUsers => {
        expect(myUsers.length).toBeGreaterThanOrEqual(0);
        expect(myUsers).toEqual(testUsers);
    });       
  }) 

  describe('API service Cleaning ', ()=>{
    afterEach(() => {
      httpMock.verify();
  });
  
  })  
  

});
