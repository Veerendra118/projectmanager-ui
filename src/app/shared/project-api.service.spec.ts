import { TestBed } from '@angular/core/testing';
import {HttpClientModule,HttpClient } from '@angular/common/http';
import { Project } from '../Model/project';
import {HttpTestingController, HttpClientTestingModule} from '@angular/common/http/testing';


import { ProjectApiService } from './project-api.service';

describe('ProjectApiService', () => {
  let service: ProjectApiService;
  let httpMock: HttpTestingController;
  const testProjects: Project[] = [{
    projectId: 2,
    priority: 1,
    startDate : new Date(2019,12,1),
    endDate : new Date(2019,12,10),
    status : 'OPEN',
    isActive :true,
    projectName: 'Jquery Prj',
    completedCount:0,
    taskCount :0,
    hasProjectDate :true,
    isProjectCompleted: false,
    managerId:1,
    managerName: "Ram" + " " +"Kaneria"
    }, 
    {
      projectId: 1,
    priority: 17,
    startDate : new Date(2019,12,1),
    endDate : new Date(2019,12,10),
    status : 'OPEN',
    isActive :true,
    projectName: 'My First Prj',
    completedCount:0,
    taskCount :0,
    hasProjectDate :true,
    isProjectCompleted: false,
    managerId:1,
    managerName: "Ram" + " " +"Kaneria"
      }
    ];

  


  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [HttpClientModule,  HttpClientTestingModule],
        providers: [ProjectApiService]
        
    });
    service = TestBed.get(ProjectApiService);
    httpMock =TestBed.get(HttpTestingController);
  });


  it('should be created', () => {
    const service: ProjectApiService = TestBed.get(ProjectApiService);
    expect(service).toBeTruthy();
  });

  it('be able to retrieve projects from the API bia GET', () => {
    service.GetAllProjects().subscribe(myprojects => {
        expect(myprojects.length).toBeGreaterThanOrEqual(0);
        expect(myprojects).toEqual(testProjects);
    });       
  });

  describe('API service Cleaning ', ()=>{
    afterEach(() => {
      httpMock.verify();
  });
  
  })  


});
