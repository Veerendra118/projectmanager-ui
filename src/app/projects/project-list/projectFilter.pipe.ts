import {Pipe, PipeTransform } from '@angular/core'
import { Project } from 'src/app/Model/project';


@Pipe({
name: 'ProjectFilter',
pure:true
})

export class ProjectFilterPipe implements PipeTransform{

transform(items: Project[], name:string ):Project[]
        {
         let  filteredProjects: Project[]=items ;
                 
           if(name.length>0)
           filteredProjects = filteredProjects
                .filter(x=>x.projectName.toLowerCase().includes(name.toLowerCase())
                                                
           );
         return filteredProjects;
    }
}