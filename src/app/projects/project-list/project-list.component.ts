import { Component, OnInit } from '@angular/core';
import {ProjectApiService} from 'src/app/shared/project-api.service';
import {Project} from 'src/app/Model/project';
import { ToastrService } from 'ngx-toastr';
import arraySort from 'array-sort';


@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

 public ProjectName : string;
 public selectedPrj : Project;
 public p :number =1;
 
  constructor(public service:ProjectApiService,private toastr: ToastrService) { 
      this.ProjectName ="";
  }

  ngOnInit() {
    this.service.refreshList();
  }

  populateForm(prj: Project) {
     
      this.service.GetProject(prj.projectId)
        .subscribe(data=>{ this.service.curProject=data;
            
          if(data.startDate !=null)
          {
            this.service.curProject.hasProjectDate=true;
            this.service.curProject.startDate = new Date(data.startDate);
            this.service.curProject.endDate = new Date(data.endDate);
            this.service.curProject.isActive =true;
            this.service.curProject.status="OPEN";
          }
        });
    
  }

  public sortByStart()
  {
      arraySort(this.service.projectList,"startDate");

  }
 public sortByEnd()
 {
   arraySort(this.service.projectList, "endDate");
 }

 public sortByPriority()
 {
    arraySort(this.service.projectList,"priority");
 }
 public sortByStatus()
 {
  arraySort(this.service.projectList, "status");

 } 
  EndOfProject(id: number) {
    if (confirm('Are you sure to suspend this  project?, Suspending project deletes all tasks associated to the project.')) {
      this.service.deleteProject(id).subscribe(res => {
        this.service.refreshList();
        this.toastr.warning('Deleted successfully', 'Project. Register');
      });
    }
  }

}
