import { Component, OnInit } from '@angular/core';
import { ProjectApiService} from 'src/app/shared/project-api.service';
import {UserApiService} from 'src/app/shared/user-api.service';
import {ToastrService} from 'ngx-toastr';
import {NgForm, Form} from '@angular/forms'
import { User } from 'src/app/Model/user';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker'
import { Project } from 'src/app/Model/Project';



@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  
  public datepickerConfig: BsDatepickerConfig;
  public managerId :number=0;
  public users : User[]=[];
  public managerName:string="";
  public managerEmpId:number=0;
  public filterUsrName :string ="";

  public disableCtrl :boolean;
    


  constructor( public service :ProjectApiService, private userService :UserApiService, private toastr:ToastrService) { 

      this.datepickerConfig = new BsDatepickerConfig(); 
      this.datepickerConfig.containerClass='theme-dark-blue';
      this.datepickerConfig.showWeekNumbers=false;
      this.datepickerConfig.dateInputFormat="DD/MM/YYYY";
      this.datepickerConfig.minDate= new Date(2019,0,1);
      this.datepickerConfig.maxDate= new Date(2020,12,31);

      this.disableCtrl= false;
      this.filterUsrName ="";
  }

  Clear(): void{
    this.resetForm();
    
  }
  ngOnInit() {
    this.resetForm();

    this.userService.GetAllUsers().subscribe
    ( data => this.users=data);
    this.filterUsrName ="";

  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.curProject = {
      projectId: 0,
      projectName: '',
      managerName: '',
      isActive: true,
      managerId :0,
      startDate :null,
      endDate :null,
      priority:0,
      status: 'OPEN',
      hasProjectDate: true,
      completedCount:0,
      taskCount:0,
      isProjectCompleted:false

    }
  this.managerEmpId=0;
  this.managerName="";
  this.managerId=0;
  this.filterUsrName ="";  
  this.disableCtrl= false;



  }

  public SetControls(item:NgForm)
  {
    
    this.disableCtrl=item.value.hasProjectDate? false: true;

    if(!item.value.hasProjectDate) 
     {
        this.service.curProject.managerId=0;
        this.service.curProject.startDate=null;
        this.service.curProject.endDate=null;
        this.service.curProject.priority=0;
        this.service.curProject.managerName="";
     }
  }



  public selectUser(event: any, item: any) {

    this.managerId=item.userId;
    this.managerEmpId=item.employeeId;
    this.service.curProject.managerId=item.userId;
    this.service.curProject.managerName=item.firstName + " "+item.lastName;
    this.managerName = this.service.curProject.managerName;
    
    this.service.curProject.status="OPEN";
    this.service.curProject.isActive=true;
  }
  public UnSelectUser()
  {
    this.managerName = "";
    this.managerId=0;
    this.managerEmpId=0;
    this.filterUsrName="";
    this.service.curProject.managerId=0;
    this.service.curProject.managerName="";
  }


  onSubmit(form: NgForm) {
    
    
    this.service.curProject.projectId =form.value.projectId;
    this.service.curProject.managerId =form.value.managerId;
    this.service.curProject.status="OPEN";
    this.service.curProject.isActive=true;
    this.service.curProject.hasProjectDate=form.value.hasProjectDate;
    
   if(this.validate(this.service.curProject))
   {

     //set values if required
     if(!this.service.curProject.hasProjectDate) 
     {
        this.service.curProject.managerId=0;
        this.service.curProject.startDate=null;
        this.service.curProject.endDate=null;
        this.service.curProject.priority=0;
     }


    if (form.value.projectId === 0 ||form.value.projectId === null)
    {
      this.service.postProject(this.service.curProject)
      .subscribe( data=>{
            this.service.refreshList();
      });
      
      this.toastr.info('Inserted successfully', 'Project. Register');
      this.resetForm(form);
    }  
    else
    {
      this.service.putProject(this.service.curProject)
      .subscribe( data=>{
            this.service.refreshList();
      });
      this.toastr.info('Updated successfully', 'Project. Register');
      this.resetForm(form);
    }
  }  
}

  
  validate(prj: Project) : boolean
  {
    // alert("project Id :"+ prj.projectId);
    // alert("manager ID:"+ prj.managerId)
    // alert("project Name :"+ prj.projectName);
    // alert("manager name :" + prj.managerName);
    // alert("hasProjectDate :" + prj.hasProjectDate);
    // alert("startdate"+ prj.startDate);
    // alert("end date:"+ prj.endDate);


    let isValid :boolean= true;
    let isExists: number =-1;

    if(prj.projectName===""||prj.projectName ===null)
    {
      this.toastr.warning("Project Name is required","Project. Register");
          isValid =false; 
          return isValid;
    }

    if(prj.hasProjectDate)
    {
        if(prj.startDate ===null || prj.endDate === null )
        {
          this.toastr.warning("Start & End dates are required","Project. Register");
          isValid =false; 
          return isValid;
        }
        if(prj.startDate >prj.endDate)
        {
          this.toastr.warning("Start should not be greater than End date","Project. Register");
          isValid =false; 
          return isValid;
        }

        if(prj.managerId ===0 || prj.managerId ===null)
        {
          this.toastr.warning("Manager is required","Project. Register");
          isValid =false; 
          return isValid;
        }
    }
    
    // update mode
    if (prj.projectId >0)
    {
      isExists =this.service.projectList
            .findIndex(x=>x.projectId !=prj.projectId && 
              x.projectName.toLowerCase().trim()===prj.projectName.toLowerCase().trim());

      if (isExists >=0)
        {
          this.toastr.warning("Project AlreadyExists","Project. Register");
          isValid =false;
        }
    }
    else // Insert mode
    {
      isExists =this.service.projectList
            .findIndex(x=>x.projectName.toLowerCase().trim()===prj.projectName.toLowerCase().trim());
      if (isExists >=0)
        {
          this.toastr.warning("Project AlreadyExists","Project. Register");
          isValid =false;
        }
    }
    return isValid;
  }

  

}
