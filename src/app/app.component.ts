import { Component } from '@angular/core';
import {Event, Router, NavigationStart,NavigationEnd, NavigationCancel} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProjectManagerUI';
   showLoadingOption: boolean=true;
  constructor( private _router:Router
  )
  {
    this._router.events.subscribe( 
      ( routerevent: Event )=>
          {
              if(routerevent  instanceof NavigationStart)  
                   this.showLoadingOption=true;
               
              if(routerevent  instanceof NavigationEnd 
                 || routerevent instanceof NavigationCancel
                )  
                   this.showLoadingOption=true;
  }  );  

  }
}
