export class Project {
    public projectId:number;
    public projectName:string;
    public managerId:number;
    public managerName:string;
    public priority: number;
    public startDate:Date;
    public endDate: Date;
    public status:string;
    public isProjectCompleted : boolean;
    public isActive : boolean;

    public taskCount :number;
    public completedCount: number;
    public hasProjectDate: boolean;
    
}
