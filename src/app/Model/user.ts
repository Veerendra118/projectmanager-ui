export class User {
    public userId:number;
    public firstName:string;
    public employeeId:number;
    public lastName:string;
    public userName:string;
    public isActive : boolean;
}
