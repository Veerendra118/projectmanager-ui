export class Task {
    public taskId:number;
    public taskName:string;
    public parentTaskId:number;
    public parentTaskName:string;
    public priority: number;
    public startDate:Date;
    public endDate: Date;
    public status:string;
    public isTaskEnded : boolean;
    public isActive : boolean;
    public projectId:number;
    public projectName:string;
    public assignedId:number;
    public taskUserName:string;
    public isParentTask:boolean;

// These properties are used for Model Window filter purpose

    public filterPrjName :string;
    public filterUsrName: string;
    public filterTskName :string;
    

}
