import { Component, OnInit } from '@angular/core';
import {UserApiService} from 'src/app/shared/user-api.service';
import {User} from 'src/app/Model/user';
import { ToastrService } from 'ngx-toastr';
import arraySort from 'array-sort';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

 public UserName : string;
 public p :number =1;
 

 

  constructor(public service:UserApiService,private toastr: ToastrService) { 
      this.UserName ="";
  }

  ngOnInit() {
    this.service.refreshList();
  }

  populateForm(usr: User) {
    this.service.currentUsr = Object.assign({}, usr);
    
  }

  public sortByFirst()
  {
     arraySort(this.service.userList, "firstName");

  }

  public sortByLast()
  {
     arraySort(this.service.userList, "lastName");
  }
  public sortByEmployeeId()
  {
     arraySort(this.service.userList, "employeeId");
  }

  EndOfUser(userId: number) {
    if (confirm('Are you sure to delete this  user?, Deleting user deletes all projects and tasks associated to the user.')) {
      this.service.deleteUser(userId).subscribe(res => {
        this.service.refreshList();
        this.toastr.warning('Deleted successfully', 'User. Register');
        
      });
    }
  }
}
