import {Pipe, PipeTransform } from '@angular/core'
import { User } from 'src/app/Model/user';


@Pipe({
name: 'UserFilter',
pure:true
})

export class UserFilterPipe implements PipeTransform{

transform(items: User[], userName:string ):User[]
        {
         let  filteredUsers: User[]=items ;
                 
           if(userName.length>0)
           filteredUsers = filteredUsers.filter(x=>x.firstName.toLowerCase().includes(userName.toLowerCase())
                                                || x.lastName.toLowerCase().includes(userName.toLowerCase())
           );
         return filteredUsers;
    }
}