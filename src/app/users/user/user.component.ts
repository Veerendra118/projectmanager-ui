import { Component, OnInit } from '@angular/core';
import { UserApiService} from 'src/app/shared/user-api.service'
import {ToastrService} from 'ngx-toastr';
import {NgForm} from '@angular/forms'
import { User } from 'src/app/Model/user';
import { findIndex } from 'rxjs/operators';




@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  
  constructor( public service :UserApiService, private toastr:ToastrService) { }

  Clear(): void{
    this.resetForm();
  }

 
  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.currentUsr = {
      employeeId: 0,
      firstName: '',
      lastName: '',
      isActive: true,
      userId :0,
      userName :''
      
    }
  }


  onSubmit(form: NgForm) {
    
    let id:number =form.value.userId;

   if(this.validate(form.value))
   { 

    if (id >0)
    {
      this.service.putUser(form.value)
            .subscribe( data => {
                this.service.refreshList();
               }
              )
              this.toastr.info('Updated successfully', 'User. Register');
              this.resetForm(form);
    }  
    else
    {
      this.service.postUser(form.value)
          .subscribe( data => 
            this.service.refreshList() );
          this.toastr.info('Inserted successfully', 'User. Register');
          this.resetForm(form)
    };
  }
}  
    
  saveRecord(form: NgForm) {
    this.service.postUser(form.value)
    .subscribe(() => {
      this.service.userList.push(this.service.currentUsr);
      this.resetForm(form);
    });
    
    this.service.GetAllUsers()
         .subscribe(data=>this.service.userList=data);
  }

  validate(usr: User) : boolean
  {
    // alert("User ID :"+ usr.userId);
    // alert("First Name :"+ usr.firstName);
    // alert("Last Name :" + usr.lastName);
    // alert("Employee Id :" + usr.employeeId);

    let isValid :boolean= true;
    let isExists: number =-1;

    //Employee Id should be a valid zero
     if( usr.employeeId ===0)
     {
      this.toastr.warning("Employee Id should be greater than 0.","User. Register");
      isValid =false;
       return isValid;
     }
    // update mode
    if (usr.userId >0)
    {
      isExists =this.service.userList
            .findIndex(x=>x.userId !=usr.userId && 
              x.firstName.toLowerCase().trim()===usr.firstName.toLowerCase().trim() && 
                      x.lastName.toLowerCase().trim()===usr.lastName.toLowerCase().trim());
      if (isExists >=0)
        {
          this.toastr.warning("User AlreadyExists","User. Register");
          isValid =false;
        }
      else
      {
          isExists =this.service.userList
                  .findIndex(x=>x.userId !=usr.userId && x.employeeId ==usr.employeeId);

           if(isExists >=0)
           {
            this.toastr.warning("Employee Id  already exists","User. Register");
            isValid =false;
           }       
      }
    }
    else // Insert mode
    {
      isExists =this.service.userList
            .findIndex(x=>x.firstName.toLowerCase().trim()===usr.firstName.toLowerCase().trim() && 
                      x.lastName.toLowerCase().trim()===usr.lastName.toLowerCase().trim());
      if (isExists >=0)
        {
          this.toastr.warning("User AlreadyExists","User. Register");
          isValid =false;
        }
      else
      {
          isExists =this.service.userList
                  .findIndex(x=>x.employeeId ===usr.employeeId);

           if(isExists >=0)
           {
            this.toastr.warning("Employee Id  already exists","User. Register");
            isValid =false;
           }       
      }
    }
    return isValid;
  }

}
