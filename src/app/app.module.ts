import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './tasks/page-not-found/page-not-found.component';
import { TaskApiService } from 'src/app/shared/task-api.service';
import {HttpClientModule} from '@angular/common/http'
import { BsDatepickerModule} from 'ngx-bootstrap/datepicker'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { TaskFilterPipe } from './tasks/view-tasks/TaskFilter.pipe';
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import { UserComponent } from './users/user/user.component';
import { UserListComponent } from './users/user-list/user-list.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectComponent } from './projects/project/project.component';
import { ProjectListComponent } from './projects/project-list/project-list.component';
import { UsersComponent } from './users/users.component';
import { UserFilterPipe } from './users/user-list/UserFilter.pipe';
import { ToastrModule } from 'ngx-toastr';
import { UserApiService } from './shared/user-api.service';
import { ProjectApiService } from './shared/project-api.service';
import { ProjectFilterPipe } from './projects/project-list/projectFilter.pipe';
import {NgxPaginationModule} from 'ngx-pagination';  
import { Ng2SearchPipeModule } from 'ng2-search-filter'; 


@NgModule({
  declarations: [
    AppComponent,
   routingComponents,
   PageNotFoundComponent,
   TaskFilterPipe,
   UserFilterPipe,
   UsersComponent,
   UserComponent,
   UserListComponent,
   ProjectsComponent,
   ProjectComponent,
   ProjectListComponent,
   ProjectFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DlDateTimeDateModule,  // <--- Determines the data type of the model
    DlDateTimePickerModule,
    BrowserAnimationsModule,
    NgxPaginationModule,Ng2SearchPipeModule,
    BsDatepickerModule.forRoot(),
    
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      
    })
    
  ],
  providers: [ TaskApiService,UserApiService,ProjectApiService
               
              ],
  bootstrap: [AppComponent]
})
export class AppModule { }

